FROM python:3.7.4-alpine3.9

ENV PYTHONUNBUFFERED 1

COPY . /djangodock

EXPOSE 8000

WORKDIR /djangodock

RUN pip install -r requirements.txt

WORKDIR /djangodock/todo

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000" ]